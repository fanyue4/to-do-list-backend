package com.tw.todoitems.db;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private Config config;

    public Connector() {
        // Need to be implemented
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("db-config.yml");
        this.config = yaml.load(inputStream);
    }

    public Config getConfig() {
        return config;
    }

    public Connection createConnect() throws SQLException {
        // Need to be implement
        return DriverManager.getConnection(this.config.getDatabaseURL(),
                this.config.getUsername(),
                this.config.getPassword());
    }

}
