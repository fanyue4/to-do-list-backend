package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;

import java.sql.*;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {
    private Connector connector;

    public ItemRepo() {
        this.connector = new Connector();
    }

    public Item addItem(Item item) {
        // Need to be implemented
        String query = "INSERT INTO Items (texts, status) VALUES(?, ?)";
        try (Connection connection = connector.createConnect();
             PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, item.getText());
            preparedStatement.setString(2, item.getStatus().name());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return findItems().get(findItems().size() - 1);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<Item> findItems() {
        // Need to be implemented
        List<Item> list = new ArrayList<>();
        String query = "SELECT * from Items";
        try (Connection connection = connector.createConnect();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String texts = resultSet.getString("texts");
                String status = resultSet.getString("status");

                Item item = new Item(id, texts, ItemStatus.valueOf(status));
                list.add(item);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public boolean updateItem(Item item) {
        // Need to be implemented
        String query = "UPDATE Items SET texts = ?, status = ? WHERE id = ?";
        try (PreparedStatement preparedStatement = connector.createConnect().prepareStatement(query)) {
            preparedStatement.setString (1, item.getText());
            preparedStatement.setString (2, item.getStatus().name());
            preparedStatement.setInt(3, item.getId());

            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean deleteItem(int id) {
        // Need to be implemented
        String query = "DELETE FROM Items WHERE id = ?";
        try (PreparedStatement preparedStatement = connector.createConnect().prepareStatement(query)) {
            preparedStatement.setInt (1, id);
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

}
