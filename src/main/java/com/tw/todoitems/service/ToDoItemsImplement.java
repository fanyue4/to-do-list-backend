package com.tw.todoitems.service;

import com.tw.todoitems.db.ItemRepo;
import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToDoItemsImplement implements ToDoItems {

    private ItemRepo itemRepo;

    public ToDoItemsImplement() {
        this.itemRepo = new ItemRepo();
    }

    @Override
    public List<Item> getAllItems() {
        return itemRepo.findItems();
    }

    @Override
    public Item createItem(Item item) {
        item.setStatus(ItemStatus.ACTIVE);
        return itemRepo.addItem(item);
    }

    @Override
    public boolean updateItem(Item item) {
        return itemRepo.updateItem(item);
    }

    @Override
    public boolean deleteItem(int id) {
        return itemRepo.deleteItem(id);
    }

}
